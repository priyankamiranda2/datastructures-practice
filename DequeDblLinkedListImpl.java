import java.util.Scanner;

public class DequeDblLinkedListImpl<T> {
 
    private Node<T> front;
    private Node<T> rear;
     
    public void insertFront(T item){
        //add element at the beginning of the queue
        System.out.println("adding at front: "+item);
        Node<T> nd = new Node<T>();
        nd.setValue(item);
        nd.setNext(front);
        if(front != null) front.setPrev(nd);
        if(front == null) rear = nd;
        front = nd;
    }
     
    public void insertRear(T item){
        //add element at the end of the queue
        System.out.println("adding at rear: "+item);
        Node<T> nd = new Node<T>();
        nd.setValue(item);
        nd.setPrev(rear);
        if(rear != null) rear.setNext(nd);
        if(rear == null) front = nd;
         
        rear = nd;
    }
     
    public void removeFront(){
        if(front == null){
            System.out.println("Deque underflow!! unable to remove.");
            return;
        }
        //remove an item from the beginning of the queue
        Node<T> tmpFront = front.getNext();
        if(tmpFront != null) tmpFront.setPrev(null);
        if(tmpFront == null) rear = null;
        System.out.println("removed from front: "+front.getValue());
        front = tmpFront;
    }
     
    public void removeRear(){
        if(rear == null){
            System.out.println("Deque underflow!! unable to remove.");
            return;
        }
        //remove an item from the beginning of the queue
        Node<T> tmpRear = rear.getPrev();
        if(tmpRear != null) tmpRear.setNext(null);
        if(tmpRear == null) front = null;
        System.out.println("removed from rear: "+rear.getValue());
        rear = tmpRear;
    }
    public void display(){
            Node current = front;
            while (current != null) {
            	System.out.println(current.value+ " ");
                current = current.next;
            }
    }
     
    public static void main(String a[]){
    	Scanner in= new Scanner(System.in);
        DequeDblLinkedListImpl<Integer> deque = new DequeDblLinkedListImpl<Integer>();
        while(true){
        System.out.println("1. insert front 2. remove front 3. insert rear 4. remove rear 5. display");
        int x= in.nextInt();
        switch(x){
        case 1: 
        	System.out.println("Enter the element to be entered:");
        	int y=in.nextInt();
        	deque.insertFront(y);
        	break;
        case 2: 
        deque.removeFront();
        break;
        case 3: 
        System.out.println("Enter the element to be entered:");
         y=in.nextInt();
        deque.insertRear(y);
        break;
        case 4: 
        deque.removeRear();
        break;
        case 5: 
        deque.display();
        break;
    }
        }
 }
 
class Node<T>{
     
    private Node<T> prev;
    private Node<T> next;
    private T value;
     
    public Node<T> getPrev() {
        return prev;
    }
    public void setPrev(Node<T> prev) {
        this.prev = prev;
    }
    public Node<T> getNext() {
        return next;
    }
    public void setNext(Node<T> next) {
        this.next = next;
    }
    public T getValue() {
        return value;
    }
    public void setValue(T value) {
        this.value = value;
    }
}
}
