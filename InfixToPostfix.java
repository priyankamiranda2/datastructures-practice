import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
class Stack1 {
    private int maxSize;
    private char[] stackArray;
    private int top;
 
    public Stack1(int max) {
        maxSize = max;
        stackArray = new char[maxSize];
        top = -1;
    }
 
    public void push(char j) {
        stackArray[++top] = j;
    }
 
    public char pop() {
        return stackArray[top--];
    }
 
    public char peek() {//shows the topmost element
        return stackArray[top];
    }
 
    public boolean isEmpty() {//checks if empty
        return (top == -1);
    }
}
class InToPost {
 private Stack1 theStack;
 private String input;
 private String output = "";
 public InToPost(String in) {
 input = in;
 int stackSize = input.length();
  theStack = new Stack1(stackSize);
 }
 
 public String doTrans() {
 for (int j = 0; j < input.length(); j++) {
 char ch = input.charAt(j);
 switch (ch) {
 case '+': 
 case '-':
 gotOper(ch, 1); 
 break; 
 case '*': 
 case '/':
 gotOper(ch, 2); 
 break; 
 case '(': 
 theStack.push(ch);
 break;
 case ')': 
 gotParen(ch); 
 break;
 default: 
 output = output + ch; 
 break;
 }
 }
 while (!theStack.isEmpty()) {
 output = output + theStack.pop();
 }
 System.out.println(output);
 return output; 
 }
 
 public void gotOper(char opThis, int prec1) {
 while (!theStack.isEmpty()) {
 char opTop = (char)theStack.pop();
 if (opTop == '(') {
 theStack.push(opTop);
 break;
 }
 else {
 int prec2;
 if (opTop == '+' || opTop == '-')
 prec2 = 1;
 else
 prec2 = 2;
 if (prec2 < prec1) { 
 theStack.push(opTop);
 break;
 }
 else
 output = output + opTop;
 }
 }
 theStack.push(opThis);
 }
 
 public void gotParen(char ch){ 
 while (!theStack.isEmpty()) {
 char chx = (char)theStack.pop();
 if (chx == '(') 
 break; 
 else
 output = output + chx; 
 }
 }
}
 
public class InfixToPostfix {
 public static void main(String[] args) throws IOException {
	//create an input stream object
			BufferedReader keyboard = new BufferedReader (new InputStreamReader(System.in));
			
			//get input from user
			System.out.print("\nEnter the algebraic expression in infix: ");
			String input = keyboard.readLine();
			
 String output;
 InToPost theTrans = new InToPost(input);
 output = theTrans.doTrans(); 
 System.out.println("Postfix is " + output );
 }
}