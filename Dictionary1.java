import java.util.*;
class Entry{
	public int key;
	public Entry(int k){
		key=k;
	}
}
class HashTable{
	Entry[]hashArray;
	int size, count;
	public HashTable(int s){
		size=s;
		count=0;
		hashArray = new Entry[size];
	}
	int hashFunc(int theKey){
		int hashVal=theKey%size;
		return hashVal;
	}
	public void insert(int theKey){
		if(!isFull()){
			int hashVal=hashFunc(theKey);
			while(hashArray[hashVal]!=null){
				++hashVal;
				hashVal%=size;
			}
			hashArray[hashVal]=new Entry(theKey);
			
			count++;
		}
		else
			System.out.println("Table is full");
	}
	public Entry search(int theKey){
		int hashVal=hashFunc(theKey);
		while(hashArray[hashVal]!=null){
			if(hashArray[hashVal].key==theKey)
				return hashArray[hashVal];
			++hashVal;
			hashVal%=size;
		}
		return null;
	}
	public void displayTable(){
		System.out.println("<<Dictionary Table>>");
		for(int i=0;i<size;i++){
			if(hashArray[i]!=null)
				System.out.println(hashArray[i].key);
		}
	}
	public boolean isFull(){
		return count==size;
	}
}
public class Dictionary1{
	public static void main(String[]args){
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the size of the table");
		int n= in.nextInt();
		HashTable ht = new HashTable(n);
		System.out.println("Enter elements");
		for(int i=0;i<n;i++)
			ht.insert(in.nextInt());
		System.out.println("What would you like to search?");
		int word = in.nextInt();
		Entry item = ht.search(word);
		if(item!=null)
			System.out.println("Found: "+item.key);
		else
			System.out.println(word+" not found");
		ht.displayTable();
	}
}