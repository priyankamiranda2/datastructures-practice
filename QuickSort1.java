import java.util.Random;
import java.util.Scanner;
class Quick_Sort 
{
    public static void sort(int[] arr)
    {
        quickSort(arr, 0, arr.length - 1);
    }
public static void quickSort(int arr[], int low, int high) 
{
    int i = low, j = high;
    int temp;
    int pivot = arr[(low + high) / 2];
    while (i <= j) 
    {
        while (arr[i] < pivot)
            i++;
        while (arr[j] > pivot)
            j--;
        if (i <= j) 
        {
            temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
            i++;
            j--;
        }
    }
    if (low < j)
        quickSort(arr, low, j);
   
    if (i < high)
        quickSort(arr, i, high);
}
}

public class QuickSort1{
	 public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Various Sort Test\n");
        int i;
        System.out.println("Sorting of randomly generated numbers using VARIOUS SORTING METHODS");
        Random random = new Random();
        System.out.println("Enter the no. of elements");
        int n = in.nextInt();
        int[] arr = new int[n];
        for (i = 0; i < n; i++)
            arr[i] = Math.abs(random.nextInt(1000));
        System.out.println("\nElements before sorting ");        
        for (i = 0; i < n; i++)
            System.out.print(arr[i]+" ");  
        System.out.println();
        int k;
        int j = (int) System.nanoTime();
        System.out.println();
Quick_Sort.sort(arr);
k = (int) System.nanoTime();
System.out.println("\n\nTime Taken ="+(k-j));
System.out.println("\nElements after sorting ");        
for (i = 0; i < n; i++)
    System.out.print(arr[i]+" ");            
System.out.println();

    }
}