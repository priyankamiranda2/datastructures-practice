
import java.util.*;
    class SelectionSort 
{
	    public static void sort( int arr[] ){
	        int N = arr.length;
	        int i, j, pos, temp;
	        for (i = 0; i < N-1; i++)
	        {
	            pos = i;
	            for (j = i+1; j < N; j++)
	            {
	                if (arr[j] < arr[pos])
	                {
	                    pos = j;
	                }
	            }
	            temp = arr[i];
	            arr[i] = arr[pos];
	            arr[pos]= temp;            
	        }        
	    }
	    
	}
    class QuickSort 
    {
        public static void sort(int[] arr)
        {
            quickSort(arr, 0, arr.length - 1);
        }
        public static void quickSort(int arr[], int low, int high) 
        {
            int i = low, j = high;
            int temp;
            int pivot = arr[(low + high) / 2];
            while (i <= j) 
            {
                while (arr[i] < pivot)
                    i++;
                while (arr[j] > pivot)
                    j--;
                if (i <= j) 
                {
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                    i++;
                    j--;
                }
            }
            if (low < j)
                quickSort(arr, low, j);
           
            if (i < high)
                quickSort(arr, i, high);
        }
    }
  class HeapSort 
	{    
	    private static int N;
	    public static void sort(int arr[])
	    {       
	        heapify(arr);        
	        for (int i = N; i > 0; i--)
	        {
	            swap(arr,0, i);
	            N = N-1;
	            maxheap(arr, 0);
	        }
	    }     
	      
	    public static void heapify(int arr[])
	    {
	        N = arr.length-1;
	        for (int i = N/2; i >= 0; i--)
	            maxheap(arr, i);        
	    }
	    public static void maxheap(int arr[], int i)
	    { 
	        int left = 2*i ;
	        int right = 2*i + 1;
	        int max = i;
	        if (left <= N && arr[left] > arr[i])
	            max = left;
	        if (right <= N && arr[right] > arr[max])        
	            max = right;
	 
	        if (max != i)
	        {
	            swap(arr, i, max);
	            maxheap(arr, max);
	        }
    }    
	    public static void swap(int arr[], int i, int j)
	    {
	        int tmp = arr[i];
	        arr[i] = arr[j];
	        arr[j] = tmp; 
	    }    
	        
	}
class MergeSort 
	{
	    public static void sort(int[] a, int low, int high) 
	    {
	        int N = high - low;         
	        if (N <= 1) 
	            return; 
	        int mid = low + N/2; 
	        sort(a, low, mid); 
	        sort(a, mid, high); 
	        int[] temp = new int[N];
	        int i = low, j = mid;
	        for (int k = 0; k < N; k++) 
	        {
	            if (i == mid)  
	                temp[k] = a[j++];
	            else if (j == high) 
	                temp[k] = a[i++];
	            else if (a[j]<a[i]) 
	                temp[k] = a[j++];
	            else 
	                temp[k] = a[i++];
	        }    
	        for (int k = 0; k < N; k++) 
	            a[low + k] = temp[k];         
	    }
	    
	}
class InsertionSort 
	{
	    public static void sort( int arr[] )
	    {
	        int N = arr.length;
	        int i, j, temp;
	        for (i = 1; i< N; i++) 
	        {
	            j = i;
	            temp = arr[i];    
	            while (j > 0 && temp < arr[j-1])
	            {
	                arr[j] = arr[j-1];
	                j = j-1;
	            }
	            arr[j] = temp;            
	        }        
	    }    
	        
	}
class Shell_Sort 
	{
	    
	 
	    public static void shellSort(int[] sequence) 
	    {
	        int increment = sequence.length / 2;
	        while (increment > 0) 
	        {
	            for (int i = increment; i < sequence.length; i++) 
	            {
	                int j = i;
	                int temp = sequence[i];
	                while (j >= increment && sequence[j - increment] > temp) 
                    {
	                    sequence[j] = sequence[j - increment];
	                    j = j - increment;
	                }
	                sequence[j] = temp;
	            }
	            if (increment == 2)
	                increment = 1;
	            else
	                increment *= (5.0 / 11);
	 
	        }
	    }
	 
	
	}
class Bucket_sort
{
static int[] sort(int[] sequence, int maxValue,int n) 
    {
        // Bucket Sort
        int[] Bucket = new int[maxValue + 1];
        int[] sorted_sequence = new int[sequence.length];
        for (int i = 0; i < sequence.length; i++)
            Bucket[sequence[i]]++;
        int outPos = 0;
        for (int i = 0; i < Bucket.length; i++)
            for (int j = 0; j < Bucket[i]; j++)
                sorted_sequence[outPos++] = i;
            return sorted_sequence; 
        
    }
    static int maxValue(int[] sequence) 
    {
        int maxValue = 0;
        for (int i = 0; i < sequence.length; i++)
            if (sequence[i] > maxValue)
                maxValue = sequence[i];
        return maxValue;
    }
}
class Bubble_Sort 
	{
	    static void sort(int[] sequence) 
	    {
	        for (int i = 0; i < sequence.length; i++)
	            for (int j = 0; j < sequence.length - 1; j++)
	                if (sequence[j] > sequence[j + 1]) 
	                {
	                    sequence[j] = sequence[j] + sequence[j + 1];
	                    sequence[j + 1] = sequence[j] - sequence[j + 1];
	                    sequence[j] = sequence[j] - sequence[j + 1];
	                }
	        
	    }
	 
	    	}



public class ds1{

    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);
        int x;
        do{
        System.out.println("Various Sort Test\n");
        int i;
        System.out.println("Sorting of randomly generated numbers using VARIOUS SORTING METHODS");
        Random random = new Random();
        System.out.println("Enter the no. of elements");
        int n = in.nextInt();
        int[] arr = new int[n];
        for (i = 0; i < n; i++)
            arr[i] = Math.abs(random.nextInt(1000));
        System.out.println("Select the sorting type you want");
        System.out.println("1.Selection Sort\n2.Heap Sort\n3.Merge Sort\n4.Insertion Sort\n5.Shell Sort\n6.Bubble Sort\n7.Bucket Sort\n8.Quick sort\n9.Exit");
        x=in.nextInt();
        System.out.println("\nElements before sorting ");        
        for (i = 0; i < n; i++)
            System.out.print(arr[i]+" ");            
        System.out.println();
        int k;
        int j = (int) System.nanoTime();
        switch(x)
        {
            case 1: 
                    SelectionSort.sort(arr);
                    k = (int) System.nanoTime();
                    System.out.println("\n\nTime Taken ="+(k-j));
                    break;
            case 2: HeapSort.sort(arr);
                    k = (int) System.nanoTime();
                    System.out.println("\n\nTime Taken ="+(k-j));
                    break;
            case 3: MergeSort.sort(arr,0,n);
                    k = (int) System.nanoTime();
                    System.out.println("\n\nTime Taken ="+(k-j));
                    break;
            case 4: InsertionSort.sort(arr);
                    k = (int) System.nanoTime();
                    System.out.println("\n\nTime Taken ="+(k-j));
                    break;
            case 5: Shell_Sort.shellSort(arr);
                    k = (int) System.nanoTime();        
                    System.out.println("\n\nTime Taken ="+(k-j));
                    break;
            case 6:
                    Bubble_Sort.sort(arr);
                    k = (int) System.nanoTime();
                    System.out.println("\n\nTime Taken ="+(k-j));
                    break;
            case 7:
            	   int maxValue;
	               maxValue = Bucket_sort.maxValue(arr);
                   k = (int) System.nanoTime();
                   System.out.println("\n\nTime Taken ="+(k-j));
                   arr=Bucket_sort.sort(arr, maxValue,n);
                   break;
       case 8:       QuickSort.sort(arr);
                    k = (int) System.nanoTime();
                    System.out.println("\n\nTime Taken ="+(k-j));
                    break;
        }
   
        System.out.println("\nElements after sorting ");        
        for (i = 0; i < n; i++)
            System.out.print(arr[i]+" ");            
        System.out.println();
    }while(x!=9);
    }
}

