import java.util.Scanner;

public class StacksUsingArrays {
	int top;
    int size;
    int[] stack ;
    public StacksUsingArrays(int arraySize){
        size=arraySize;
        stack= new int[size];
        top=-1;
    }

    public void push(int value){
        if(top==size-1){
            System.out.println("Stack is full, can't push a value");
        }
        else{

            top=top+1;
            stack[top]=value;
        }
    }

    public void pop(){
        if(top!=-1)
            top=top-1;
        else{
            System.out.println("Can't pop...stack is empty");
        }
    }


    public void display(){

        for(int i=0;i<=top;i++){
            System.out.print(stack[i]+ " ");
        }
        System.out.println();
    }
    public static void main(String[] args) {
       Scanner in= new Scanner (System.in);
       System.out.println("Implementation of stacks using arrays");
       System.out.println("Enter size of the array");
       int size=in.nextInt();
       StacksUsingArrays newStack = new StacksUsingArrays(size);
       int x=9;
       while(x!=0){
        System.out.println("Choose operation");
         x=in.nextInt();
        switch(x){
        case 1:
        	int value=in.nextInt();
        	newStack.push(value);
        	break;
        case 2: newStack.pop();
        	case 3: newStack.display();
        }
        }
    }
}
